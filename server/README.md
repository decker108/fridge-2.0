This is the server for the Fridge 2.0 Android app, written in Clojure.

Get dependencies:
    lein deps

Start dev server:
    lein ring server

Create .war file:
    lein ring uberwar
