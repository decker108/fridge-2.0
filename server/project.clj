(defproject fridge-api "0.1.0"
  :description "The Fridge 2.0 API"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [compojure "1.1.1"]
                 [yesql "0.4.0"]
                 [hiccup "1.0.0"]
                 [postgresql/postgresql "8.4-702.jdbc4"]]
  :plugins [[lein-ring "0.7.1"]]
  :ring {:handler fridge.api.routes/app}
  :resource-paths ["src/fridge/resources"]
)
