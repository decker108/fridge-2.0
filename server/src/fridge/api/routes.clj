(ns fridge.api.routes
  (:use compojure.core
        fridge.api.views
        [hiccup.middleware :only (wrap-base-url)])
  (:require [compojure.route :as route]
            [compojure.handler :as handler]
            [compojure.response :as response]))

(defroutes main-routes
  (GET "/" [] (index-page))
  (GET "/getChanges" [] (getChanges 0))
  (GET "/getChanges/" [] (getChanges 0))
  (GET ["/getChanges/:date", :date #"[0-9]+"] [date] (getChanges date))
  (GET ["/getChanges/:date/", :date #"[0-9]+"] [date] (getChanges date))
  (GET "/getRemovals" [] (getRemovals 0))
  (GET "/getRemovals/" [] (getRemovals 0))
  (GET ["/getRemovals/:date", :date #"[0-9]+"] [date] (getRemovals date))
  (GET ["/getRemovals/:date/", :date #"[0-9]+"] [date] (getRemovals date))
  (GET "/getInventory" [] (getInventory))
  (GET "/getInventory/" [] (getInventory))
  (POST ["/addItem/:barcode", :barcode #"[0-9]+"] [barcode] (addItem barcode))
  (POST ["/addItem/:barcode/", :barcode #"[0-9]+"] [barcode] (addItem barcode))
  (POST ["/removeItem/:barcode", :barcode #"[0-9]+"] [barcode] (removeItem barcode))
  (POST ["/removeItem/:barcode/", :barcode #"[0-9]+"] [barcode] (removeItem barcode))
  (route/resources "/")
  (route/not-found "Page not found"))

(def app
  (-> (handler/site main-routes)
      (wrap-base-url)))
