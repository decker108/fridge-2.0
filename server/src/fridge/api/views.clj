(ns fridge.api.views
  (:use [hiccup core page])
  (:require [clojure.java.jdbc :as jdbc]
			[yesql.core :refer :all])
)

(def db-spec {:classname "org.postgresql.Driver"
              :subprotocol "postgresql"
              :subname "//localhost:5432/fridgedb"
              :user "fridgedbuser"
              :password "kylskap"})

(defqueries "queryfile.sql")

(defn index-page []
  (html5
    [:head
      [:title "Hello World"]]
    [:body
      [:h1 "Hello World"]]))

(defn makeListFromQueryResult [outputList resultSet]
	(if (empty? resultSet)
		(str outputList)
		(do 
			(def row (hash-map :event_date (get resultSet :event_date), :barcode (get resultSet :barcode), :type (get resultSet :type)))
			(makeListFromQueryResult (into outputList row) (rest (split-at 6 resultSet)))
		)
	)
)

(defn getChanges [date]
	(if (> (Integer. date) 0) 
		(str (get-all-events db-spec date))) ;TODO date need clj-time to coerce from long to date
		(str (makeListFromQueryResult [] (get-latest-events db-spec)) \newline)
		;(println (type (first (get-latest-events db-spec)))) ;yesql returns PersistentArrayMap
)

(defn getRemovals [date]
	(if (> (Integer. date) 0) 
		(str "removals since data")
		(str "removals since start"))	
)

(defn getAdditions [date]
	(if (> (Integer. date) 0) 
		(str "removals since data")
		(str "removals since start"))	
)

(defn getInventory []
	(println (get-inventory db-spec))
	(str "inventory..." \newline)
	;(get-inventory db-spec 123)
)

(defn addItem [barcode]
	(str "adding item with barcode: " barcode \newline)
	;(jdbc/with-db-transaction [connection db-spec]
    ;	{:blaha (new-ware-event! connection 123)})
	(new-ware-event! db-spec 123)
)

(defn removeItem [barcode]
	(str "removing item with barcode: " barcode \newline)
)
