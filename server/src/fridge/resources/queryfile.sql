-- name: get-inventory
-- Gets the items currently stored in the fridge.
SELECT barcode, SUM(type) FROM t_events GROUP BY barcode

-- name: count-current-inventory
-- Counts all the items currently stored in the fridge.
SELECT SUM(qty) FROM (SELECT SUM(type) AS qty FROM t_events GROUP BY barcode) AS innerQuery WHERE qty > 0

-- name: new-ware-event!
-- Adds event for inserting a ware into the fridge.
INSERT INTO t_events(type, barcode, event_date) values(1, :barcode, current_timestamp)

-- name: remove-ware-event!
-- Adds event for removing a ware from the fridge.
INSERT INTO t_events(type, barcode, event_date) values(-1, :barcode, current_timestamp)

-- name: get-all-events
SELECT * FROM t_events;

-- name: get-latest-events
SELECT type, barcode, event_date FROM t_events ORDER BY event_date DESC LIMIT 1

-- name: get-events-since-date
SELECT type, barcode, event_date FROM t_events WHERE event_date > :sinceDate ORDER BY event_date DESC LIMIT 5

-- name: test-query
SELECT current_timestamp,123;
